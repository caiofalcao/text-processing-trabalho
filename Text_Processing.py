import nltk
import re
import calendar
import sys
import numpy as np
import random
import string
import heapq
import bs4 as bs
import urllib.request
import re
from datetime import datetime
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from textblob import TextBlob
from nltk.stem.porter import PorterStemmer
from nltk.stem.wordnet import WordNetLemmatizer
from num2words import num2words
import os

import importlib

#moduleName = input('emo_unicode.py')
#importlib.import_module(moduleName)

nltk.download('stopwords')
nltk.download('punkt')
nltk.download('webtext')
nltk.download('wordnet')

# Create Folder Result
folderOutput = 'Output_Texts'
if not os.path.exists('Output_Texts'):
    os.makedirs('Output_Texts')

# Save and Read File Text
def saveFile(name, text):

    file = open(folderOutput +"\\"+name + ".txt", "w")
    file.write(str(text))
    file.close()

'''
1.	Tokenization
'''
#a)	Generates a list of tokenized words
def tokenize(text):
    tokenizedText = word_tokenize(text)
    saveFile("1.Tokenization",tokenizedText)
    print("1.Tokenization - a)	Generates a list of tokenized words")
    return tokenizedText
'''
2.	Cleaning
'''
#a)	Lower casing
def lowerCasing(text):
    lower_text = text.lower()
    saveFile("2.Cleaning - Lower casing",lower_text)
    print("2.Cleaning - Lower casing")

#b)	Removal of Punctuations
def removalPunctuations(text):
    words = nltk.word_tokenize(text)
    new_words = [word for word in words if word.isalnum()]
    saveFile("2.Cleaning - Removal of Punctuations",new_words)
    print("2.Cleaning - Removal of Punctuations")

#c)	Removal of Stopwords
def stopWordsRemoval(text):
    text_tokens = tokenize(text);
    tokens_without_sw = [word for word in text_tokens if not word in stopwords.words()]
    saveFile("2.Cleaning - Removal of Stopwords",tokens_without_sw)
    print("2.Cleaning - Removal of Stopwords")

#d)	Removal of Frequent words e)	Removal of Rare words
def removalFrequentWords_RareWords(text):
    wordsTokenized = tokenize(text)
    data_analysis = nltk.FreqDist(wordsTokenized)
    frequencyList = data_analysis.most_common()
    MaxfrequentWord=[]
    rareWords = []

    for f in frequencyList:
        if f[1] <= 2:
            rareWords.append(f[0])
        else:
            MaxfrequentWord.append(f[0])

    removeMaxfrequentWord = list(filter(lambda a: a != MaxfrequentWord[0], wordsTokenized))
    removeRareWords = list(filter(lambda a: a != rareWords[0], removeMaxfrequentWord))
    saveFile("2.Cleaning - Removal of Frequent words - Removal of Rare words",removeRareWords)
    print("2.Cleaning - Removal of Frequent words - Removal of Rare words")
#f)	Removal of emojis   g)	Removal of emoticons
def remove_emoji(text):
    emoji_pattern = re.compile(pattern = "["
        u"\U0001F600-\U0001F64F"  # emoticons
        u"\U0001F300-\U0001F5FF"  # symbols & pictographs
        u"\U0001F680-\U0001F6FF"  # transport & map symbols
        u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
                           "]+", flags = re.UNICODE)
    result = emoji_pattern.sub(r'',text)
    saveFile("2.Cleaning - Removal of emojis - Removal of emoticons",result)
    print("2.Cleaning - Removal of emojis - Removal of emoticons")
#h)	Removal of URLs
def remove_URL(text):
    url = re.compile(r"https?://\S+|www\.\S+")
    result =  url.sub(r"",text)
    saveFile("2.Cleaning - Removal of URLs", result)
    print("2.Cleaning - Removal of URLs")

#Removal of HTML tags/markups
def remove_html(text):
    html = re.compile(r"<.*?>")
    result = html.sub(r"",text)
    saveFile("2.Cleaning - Removal of HTML tags - markups", result)
    print("2.Cleaning - Removal of HTML tags - markups")
#chatWordConversation
def chatWordConversation(text):
    chat_words_map_dict = {}
    chat_words_list = []
    f = open("chat_words_str.txt", 'r')
    chat_words_str = f.read()
    for line in chat_words_str.split("\n"):
        if line != "":
            cw = line.split("=")[0]
            cw_expanded = line.split("=")[1]
            chat_words_list.append(cw)
            chat_words_map_dict[cw] = cw_expanded
    chat_words_list = set(chat_words_list)

    new_text = []
    for w in text.split():
        if w.upper() in chat_words_list:
            new_text.append(chat_words_map_dict[w.upper()])
        else:
            new_text.append(w)
    result =  " ".join(new_text)
    saveFile("2.Cleaning - Chat words conversion", result)
    print("2.Cleaning - Chat words conversion")
#Spelling correction
def spellingCorrection(text):
    spellingCorrection = TextBlob(text)

    spellingCorrectionResult = str(spellingCorrection.correct())
    #print("corrected text: " + spellingCorrectionResult)
    result = spellingCorrectionResult
    saveFile("2.Cleaning - Spelling correction", result)
    print("2.Cleaning - Spelling correction")
'''3 - Normalization'''

#Converting dates to text and #Number to Text
def convertingDatesToText(text):
    #Pos Tagging date identification
    lines = [line.strip() for line in text.split("\n") if len(line) > 0]
    lines = [nltk.word_tokenize(line) for line in lines]
    lines = [nltk.pos_tag(line) for line in lines]
    #Convert Date Number to String Date
    itens = []

    for temp in range(len(lines)):
        a = len(lines[temp])
        for temp2 in range(a):
            #print(lines[temp][temp2])
            itens.append(lines[temp][temp2])
    cdList=[]
    for i in range(len(itens)):
        #for j in range(0,1):
        if itens[i][1] == 'CD':
            cdList.append([itens[i][0],i])

    tempList=[]

    for i in range(len(cdList)):
        splitedDate = str(cdList[i][0]).split('/')
        if len(splitedDate) > 1:
            if splitedDate[1].isnumeric():
                newDate = num2words(int(splitedDate[1]), to='ordinal')
                newDate = newDate + " " + calendar.month_name[int(splitedDate[0])]
                newDate = newDate + " " + num2words(int(splitedDate[2]), to='year')
                tempList.append(newDate)


    for i in range(len(tempList)):
        itens[cdList[i][1]] = [tempList[i],'CD']

    textModify=""
    for i in itens:
        textModify = textModify + (i[0]) + " "
    result =  textModify
    saveFile("3.Normalization - Converting dates to text", result)
    print("3.Normalization - Converting dates to text")
def convertingNumberToText(text):
    #Pos Tagging date identification
    lines = [line.strip() for line in text.split("\n") if len(line) > 0]
    lines = [nltk.word_tokenize(line) for line in lines]
    lines = [nltk.pos_tag(line) for line in lines]
    #Convert Date Number to String Date
    itens = []

    for temp in range(len(lines)):
        a = len(lines[temp])
        for temp2 in range(a):
            #print(lines[temp][temp2])
            itens.append(lines[temp][temp2])
    cdList=[]
    for i in range(len(itens)):
        #for j in range(0,1):
        if itens[i][1] == 'CD':
            cdList.append([itens[i][0],i])

    tempList=[]

    for i in range(len(cdList)):
        if cdList[i][0].isnumeric():
            newDate = num2words(int(cdList[i][0]), to='ordinal')
            tempList.append(newDate)
        else:
            tempList.append("")

    for i in range(len(tempList)):
        itens[cdList[i][1]] = [tempList[i],'CD']

    textModify=""
    for i in itens:
        textModify = textModify + (i[0]) + " "
    result =  textModify
    saveFile("3.Normalization - Converting Number to text", result)
    print("3.Normalization - Converting Number to text")

'''################ 4 - Lemmatization #################'''
def lematization(text):
    tokens = tokenize(text)
    text = [WordNetLemmatizer().lemmatize(i) for i in tokens]
    result = " ".join(text)
    saveFile("4.Lemmatization", result)
    print("4.Lemmatization")
'''################ 5 - Stemming #################'''
def stemming(text):
    tokens = tokenize(text)
    text = [PorterStemmer().stem(word) for word in tokens]
    result = " ".join(text)
    saveFile("5.Stemming", result)
    print("5.Stemming")
'''################ 6 - Convert text data to vectors of numbers(BoW) #################'''
def bagOfWord(text):
    raw_html = urllib.request.urlopen('https://en.wikipedia.org/wiki/Natural_language_processing')
    raw_html = raw_html.read()

    article_html = bs.BeautifulSoup(raw_html, 'lxml')

    article_paragraphs = article_html.find_all('p')

    article_text = ''

    for para in article_paragraphs:
        article_text += para.text

    corpus = nltk.sent_tokenize(article_text)
    for i in range(len(corpus)):
        corpus[i] = corpus[i].lower()
        corpus[i] = re.sub(r'\W', ' ', corpus[i])
        corpus[i] = re.sub(r'\s+', ' ', corpus[i])
    wordfreq = {}
    for sentence in corpus:
        tokens = nltk.word_tokenize(sentence)
        for token in tokens:
            if token not in wordfreq.keys():
                wordfreq[token] = 1
            else:
                wordfreq[token] += 1
    most_freq = heapq.nlargest(200, wordfreq, key=wordfreq.get)
    sentence_vectors = []
    for sentence in corpus:
        sentence_tokens = nltk.word_tokenize(sentence)
        sent_vec = []
        for token in most_freq:
            if token in sentence_tokens:
                sent_vec.append(1)
            else:
                sent_vec.append(0)
        sentence_vectors.append(sent_vec)
    sentence_vectors = np.asarray(sentence_vectors)
    saveFile("6.BoW - Converting text data to vectors of numbers", wordfreq)
    print("6 - Convert text data to vectors of numbers(BoW)")

def executeAll(text):
    tokenize(text)
    lowerCasing(text)
    removalPunctuations(text)
    stopWordsRemoval(text)
    removalFrequentWords_RareWords(text)
    remove_emoji(text)
    remove_URL(text)
    remove_html(text)
    chatWordConversation(text)
    spellingCorrection(text)
    convertingDatesToText(text)
    convertingNumberToText(text)
    lematization(text)
    stemming(text)
    bagOfWord(text)

def main():

    fileText = sys.argv[1]
    option = sys.argv[2]
    f = open(fileText, 'r')

    text = f.read()

    switcher = {
        0: executeAll,
        1: tokenize,
        2: lowerCasing,
        3: removalPunctuations,
        4: stopWordsRemoval,
        5: removalFrequentWords_RareWords,
        6: remove_emoji,
        7: remove_URL,
        8: remove_html,
        9: chatWordConversation,
        10: spellingCorrection,
        11: convertingDatesToText,
        12: convertingNumberToText,
        13: lematization,
        14: stemming,
        15: bagOfWord
    }

    switcher[int(option)](text)


if __name__ == "__main__":
    main()