Universidade Federal do Maranhão
Doutorado em Ciência da Computação
Disciplina: Processamento de Linguagem Natural
Professor: Anselmo Cardoso de Paiva
Aluno: Caio Eduardo Falcão Matos

Instruções:

Comando para Execução:

	python Text_Processing.py [fileText] [Option]

[fileText]
	Arquivo contendo o texto a ser processado
[Option]
	0: executeAll,
        1: tokenize,
        2: lowerCasing,
        3: removalPunctuations,
        4: stopWordsRemoval,
        5: removalFrequentWords_RareWords,
        6: remove_emoji,
        7: remove_URL,
        8: remove_html,
        9: chatWordConversation,
        10: spellingCorrection,
        11: convertingDatesToText,
        12: convertingNumberToText,
        13: lematization,
        14: stemming,
        15: bagOfWord